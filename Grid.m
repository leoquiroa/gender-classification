function H = Grid(C,varargin)
%% optagrin
    optargin = size(varargin, 2);
    i = 1;
    while (i <= optargin)
        if (strcmp(varargin{i}, 'grid'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                grid = varargin{i+1};
                i = i + 2;
            end
        elseif (strcmp(varargin{i}, 'sizeHist'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                sizeHist = varargin{i+1};            
                i = i + 2;
            end 
        elseif (strcmp(varargin{i}, 'GaussHist'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                GaussHist = varargin{i+1};            
                i = i + 2;
            end   
        elseif (strcmp(varargin{i}, 'typeMethod'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                typeMethod = varargin{i+1};            
                i = i + 2;
            end  
        else
            error('argument %s is does not exist',varargin{i});        
            i = i + 2;
        end  
    end    
%% Compute code
%   if strcmp(typeMethod,'LBP')
%     [C] = LBP(I,1)+1;
%   elseif strcmp(typeMethod,'LTP')
%     [C] = LTP(I,1)+1;
%     sizeHist = sizeHist/2;
%   elseif or(strcmp(typeMethod,'LDP'), strcmp(typeMethod,'LDN')) 
%     [C] = fcode(I,M);
%   end
    r = size(C,1);
    c = size(C,2);
    if GaussHist == -1 
      gaussM = 0; 
    else
      gaussM =  size(GaussHist,1); 
    end
    H = cell(length(grid),gaussM+1);    
    for g = 1:length(grid)
      imgCell = calculateCell(C,typeMethod,r,c,grid(g));  
      % compute the histogram
      fhist = @(x) calculateHist(x,sizeHist);
      posHs = cellfun(fhist,imgCell,'UniformOutput',false);
      tH = cell2mat(posHs(:)');
      H{g,1} = tH;
      % applies the gaussian effect to the histogram
      for gf = 1:gaussM
        fGhist = @(y) HistGauss(y(:),GaussHist(gf,:),sizeHist);
        fgaussHs = cellfun(fGhist,posHs,'UniformOutput',false);
        tH = cell2mat(fgaussHs(:)');
        H{g,gf+1} = tH;
      end
    end
end %function

function CC = calculateCell(C,kindMethod,r,c,grid)
  rw = round(r/grid);
  cw = round(c/grid);

  ri = ones(1,grid).*rw;
  ci = ones(1,grid).*cw;

  sumRi = sum(ri(:));
  if sumRi<r || sumRi>r
      ri(end) = ri(end)+r-sumRi;
  end
  sumCi = sum(ci(:));
  if sumCi<c || sumCi>c
      ci(end) = ci(end)+c-sumCi;
  end
  
  if strcmp(kindMethod,'LBP') || strcmp(kindMethod,'LDP') || strcmp(kindMethod,'LDN')
    CC = mat2cell(C,ri,ci);
  elseif strcmp(kindMethod,'LTP')
    C1 = mat2cell(C(:,:,1),r,c);
    C2 = mat2cell(C(:,:,2),r,c);
    CC = [C1; C2];
  end
end

function HH = calculateHist(D,sizeHistEdge)
    HH = hist(D(:),1:sizeHistEdge);
    % line(4[1:4]), chicken(8[5:12]), corners(8[13:20]), adjacent(8[21:28])
    H1 = HH(1:4);
    H1 = (H1/sum(H1(:)))*100;
    H2 = HH(5:12);
    H2 = (H2/sum(H2(:)))*100;
    H3 = HH(13:20);
    H3 = (H3/sum(H3(:)))*100;
    H4 = HH(21:28);
    H4 = (H4/sum(H4(:)))*100;
    HH = [H1 H2 H3 H4];
end

function gH = HistGauss(h,m,sizeHist)
    % multiplies the mask to operate it like a list
    m = repmat(m,sizeHist,1);
    % a list in left and right of one bin
    colors = 8;
    permCode = sizeHist/colors;
    listHist = zeros(permCode,size(m,2));
    % fill with elements in 'side'
    listHist(:,2) = 1:permCode;                                     % center
    listHist(:,1) = mod(listHist(:,2)-(colors+1),permCode)+1;       % left    
    listHist(:,3) = mod(listHist(:,2)+(colors-1),permCode)+1;       % right
    % repeat the list 'color' times
    listHist = repmat(listHist,colors,1);
    % create a offset list for the previous list
    offsetList = zeros(permCode,1);
    for a = 1:colors
        inx = permCode*(a-1)+1;
        offsetList(inx:inx+permCode-1) = zeros(permCode,1) + (a-1)*permCode;
    end
    % repeat the list 'side' times
    offsetList = repmat(offsetList,1,size(m,2));
    % merge
    listHist = listHist + offsetList;
    % replace the isolate value for 3 new values
    h = h(listHist);
    % multiplies the list of bin with the mask
    gHd = h.*m;
    h = sum(gHd,2)';
    % merge both histograms normalized
    gH = (h/sum(h))*1000;
end