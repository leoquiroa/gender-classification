function Classifier(varargin)
%% optagrin
    optargin = size(varargin, 2);
    i = 1;
    while (i <= optargin)
        if (strcmp(varargin{i}, 'hfMet'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                hfMet = varargin{i+1};            
                i = i + 2;
            end
        elseif (strcmp(varargin{i}, 'hfHist'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                hfHist = varargin{i+1};            
                i = i + 2;
            end
        elseif (strcmp(varargin{i}, 'PathDB'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                PathDB = varargin{i+1};            
                i = i + 2;
            end
        elseif (strcmp(varargin{i}, 'gTruth'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                gTruth = varargin{i+1};            
                i = i + 2;
            end
        elseif (strcmp(varargin{i}, 'subDB'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                subDB = varargin{i+1};            
                i = i + 2;
            end
        elseif (strcmp(varargin{i}, 'PathRoot'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                PathRoot = varargin{i+1};            
                i = i + 2;
            end
        elseif (strcmp(varargin{i}, 'PathResults'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                PathResults = varargin{i+1};            
                i = i + 2;
            end
        elseif (strcmp(varargin{i}, 'extension'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                extension = varargin{i+1};            
                i = i + 2;
            end
        elseif (strcmp(varargin{i}, 'typeMethod'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                typeMethod = varargin{i+1};            
                i = i + 2;
            end
        elseif (strcmp(varargin{i}, 'typeMask'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                typeMask = varargin{i+1};            
                i = i + 2;
            end
        elseif (strcmp(varargin{i}, 'totalAngle'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                totalAngle = varargin{i+1};
                i = i + 2;
            end
        elseif (strcmp(varargin{i}, 'dirsInMask'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                dirsInMask = varargin{i+1};            
                i = i + 2;
            end  
        elseif (strcmp(varargin{i}, 'sizeM'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                sizeM = varargin{i+1};            
                i = i + 2;
            end
        elseif (strcmp(varargin{i}, 'spacesM'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                spacesM = varargin{i+1};            
                i = i + 2;
            end 
        elseif (strcmp(varargin{i}, 'kProminent'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                kProminent = varargin{i+1};
                i = i + 2;
            end
        elseif (strcmp(varargin{i}, 'sizeHist'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                sizeHist = varargin{i+1};            
                i = i + 2;
            end    
        elseif (strcmp(varargin{i}, 'grid'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                grid = varargin{i+1};
                i = i + 2;
            end
        elseif (strcmp(varargin{i}, 'GaussHist'))
            if (i >= optargin)
                error('argument required for %s', varargin{i});
            else
                GaussHist = varargin{i+1};
                i = i + 2;
            end
        else
            error('argument %s is does not exist',varargin{i});        
            i = i + 2;
        end
    end    
    
%% Get grand truth of images 
  fl = load(strcat(PathRoot,'faceListTotal.mat'));
  faceList = fl.faceList;
%   flf = load(strcat(PathRoot,'faceListFeret.mat'));
%   faceList = flf.faceList;
%   lng = load(strcat(PathRoot,'ListNumGender.mat'));
%   ListNumGender = lng.ListNumGender;
%   ListNumGender(ListNumGender==1)=-1;
%   ListNumGender(ListNumGender==0)=1;
%   
%   for a = 1:length(faceList)
%     faceList{a,4} = ListNumGender(a);
%   end    
    %% select the mask
    if strcmp(typeMask,'Sobel')
        nM = createSobelMask(sizeM);
    elseif strcmp(typeMask,'Kirsch')
        nM = createKirschMask(sizeM);
    elseif strcmp(typeMask,'Gaussian')
        nM = createGaussGradient(sizeM);
    end
    rM = rotateMask(nM,totalAngle,dirsInMask);
    if spacesM > 0
      sM = insertSpacesMask(rM,spacesM);
    else
      sM = rM;
    end
    %% Pre-processing
     totalPerson = size(faceList,1);
     if GaussHist == -1
      gaussM = 0;
     else
      gaussM =  size(GaussHist,1);
     end
     gridSize = length(grid);
    %to create the cell matrix with all the results
    MatTotal = cell(gridSize,gaussM+1);    
    for g = 1:gridSize
      for gf = 1:gaussM+1
        MatTotal{g,gf} = zeros(totalPerson,sizeHist*grid(g)*grid(g)*3);
      end
    end
    
    %to store the set of histograms
    for idFace = 1:totalPerson
      fprintf('idPerson: %d\n', idFace);
      makePathFile = strcat(PathDB,faceList{idFace,1},'/',faceList{idFace,2});                %Linux
      Img = imread(makePathFile); % read from files the image
      % Create coded image
      C1 = hfMet(imresize(Img,0.5),sM);
      H1 = hfHist(C1);
      C2 = hfMet(Img,sM); 
      H2 = hfHist(C2);
      C3 = hfMet(imresize(Img,1.5),sM);
      H3 = hfHist(C3);
      
      for g = 1:gridSize
        for gf = 1:gaussM+1
          tVec = [H1{g,gf} H2{g,gf} H3{g,gf}];
          tVec(isnan(tVec))=0;
          MatTotal{g,gf}(idFace,:) = tVec;
        end
      end
    end

%   creating the generic name
    genericName = strcat(PathResults,...
         typeMethod,...
          '.',typeMask,...
          '.sizeM',num2str(sizeM),...
          '.spacesM',num2str(spacesM),...
          '.k',num2str(kProminent));
    %saving to disk the whole set of histograms in disk
    for g = 1:gridSize
     for gf = 1:gaussM+1
        tMatTotal = MatTotal{g,gf};
        finalName = strcat(genericName,'.grid',num2str(grid(g)),'.GHist',num2str(gf),'.file.mat');
        save(finalName,'tMatTotal');
     end
    end
    
%     % generating the SVM files
%     % men = -1
%     % women = 1
%     fprintf('Creating Files for SVM');
%     for g = 1:gridSize
%       sizeH = sizeHist*grid(g)*grid(g);
%       for gf = 1:length(GaussHist)+1
%         fprintf('grid: %d, GaussH: %d\n', grid(g),gf);
%         ref = strcat(genericName,'.grid',num2str(grid(g)),'.GHist',num2str(gf));
%         mt = load(strcat(ref,'.fileKNN.mat'));
%         MatTotal = mt.tMatTotal;
%         ListSVM = cell(0);
%         for idFace = 1:totalPerson
% %           fprintf('idFace: %d\n', idFace);
%           ListSVM{idFace} = num2str(ListNumGender(idFace));
%           for j = 1:sizeH
%             char_ix = num2str(j);
%             ListSVM{idFace} = [ListSVM{idFace},' ',char_ix,':',num2str(MatTotal(idFace,j))];    
%           end
%         end
%         fid = fopen(strcat(ref,'.fileSVM.txt'), 'w');
%         fprintf(fid,'%s\n',ListSVM{:});
%         fclose(fid);
%       end
%     end
  
end

%% %%%%%%%%%%%%%%%%%%%%%%%% creating masks %%%%%%%%%%%%%%%%%%%%%%%%%%
function nM = createSobelMask(wideMask)
    sH = (wideMask-1)/2;
    %
    xcomp = zeros(wideMask,1);
    list = zeros(1,sH);
    for i = 0:sH-1
        list(i+1) = power(2,i);
    end
    xcomp(1:sH) = list;
    xcomp(sH+1) = 0;
    xcomp(sH+2:wideMask) = fliplr(-list);
    %
    ycomp = ones(1,wideMask);
    list = zeros(1,sH-1);
    for i = 1:sH-1
        if i==1
            list(i) = power(2,sH);
        else
            list(i) = list(i-1)+2;
        end
    end
    ycomp(2:sH) = list;
    ycomp(sH+2:wideMask-1) = fliplr(list);
    ycomp(sH+1) = ycomp(sH)+ycomp(sH+2);
    nM = xcomp*ycomp;
end

function nM = createKirschMask(wideMask)
    sH = (wideMask-1)/2;
    OddList = fliplr(1:2:wideMask-1);
    EvenList = fliplr(2:2:wideMask-1);
    nM = zeros(wideMask,wideMask);
    %positive side
    list = 5:6:sH*6;
    MatPos = repmat(list,wideMask,1);
    %negative side
    list = 3:2:wideMask;
    list = fliplr(-list);
    MatNeg = repmat(list,wideMask,1);
    %left-right side
    nM(:,1:sH) = MatNeg(:,:);
    nM(:,sH+2:wideMask) = MatPos(:,:);
    %up-bottom side
    for a = 1:sH
        nM(a,a+1:OddList(a)+a) = list(a);
        nM(a+EvenList(a),a+1:OddList(a)+a) = list(a);
    end    
end

function nM = createGaussGradient(sigma)
    epsilon=1e-2;
    halfsize=ceil(sigma*sqrt(-2*log(sqrt(2*pi)*sigma*epsilon)));
    sizeM=2*halfsize+1;
    %generate a 2-D Gaussian kernel along x direction
    hx = zeros(sizeM,sizeM);
    for i=1:sizeM
        for j=1:sizeM
            u=[i-halfsize-1 j-halfsize-1];
            % change the shift of the gaussian
            hx(i,j)=gauss(u(2),sigma)*dgauss(u(1),sigma);
        end
    end
    nM=hx/sqrt(sum(sum(abs(hx).*abs(hx))));
end

function y = gauss(x,sigma) %Gaussian
    y = exp(-x^2/(2*sigma^2)) / (sigma*sqrt(2*pi));
end

function y = dgauss(x,sigma) %first order derivative of Gaussian
    y = -x * gauss(x,sigma) / sigma^2;
end

%% %%%%%%%%%%%%%%%%%%%%%%%% rotating the mask %%%%%%%%%%%%%%%%%%%%%%%%%%
function rM = rotateMask(nM,totalAngle,dirsInMask)    
    wideMask = size(nM,1);
    sH = (wideMask-1)/2;
    EvenList = 2:2:dirsInMask;
    rangeJump = totalAngle/dirsInMask;
    rangeJump = rangeJump:rangeJump:totalAngle-1;
    rM(:,:,1) = nM;
    for k = 1:length(rangeJump)
        if ismember(k,EvenList)
            rM(:,:,k+1) = imrotate(nM,rangeJump(k),'crop');
        else
            rM(sH:sH+2,sH:sH+2,k+1) = imrotate(nM(sH:sH+2,sH:sH+2),rangeJump(k),'crop');            
        end
    end
    sides = 4;
    around = 0;
    while around<(sH-1)
        Mag = wideMask-(around*2);  
        sizeTable = (Mag-1)*sides;
        oldT = zeros(sizeTable,2);
        newT = zeros(sizeTable,2,sides);
        oldT(1:end,1) = [fliplr(2:Mag) ones(1,Mag-1) 1:Mag-1 ones(1,Mag-1)*Mag] + around;
        oldT(1:end,2) = [ones(1,Mag-1)*Mag fliplr(2:Mag) ones(1,Mag-1) 1:Mag-1] + around;
        iter = zeros(sides,2);
        for ixS = 1:sides
            iter(ixS,1) = sH + (Mag-1)*(ixS-1) - around;
            iter(ixS,2) = sizeTable - iter(ixS,1) + 1;
            newT(1:iter(ixS,2)-1,:,ixS) = oldT(iter(ixS,1)+1:sizeTable,:);
            newT(iter(ixS,2):sizeTable,:,ixS) = oldT(1:iter(ixS,1),:);
        end
        for ixS = 1:dirsInMask/2
            for ixChg = 1:length(oldT)
                rM(newT(ixChg,1,ixS),newT(ixChg,2,ixS),ixS*2) = rM(oldT(ixChg,1),oldT(ixChg,2),1);
            end
        end
        around = around+1;
    end
end

%% %%%%%%%%%%%%%%%%%% including jumps between pixels %%%%%%%%%%%%%%%%%%%
function sM = insertSpacesMask(rM,spaces)
    %create the index for empty pixels
    if spaces > 0
        wideMask = size(rM,1);
        jumpMask = size(rM,3); 
        spaceSize = wideMask+(wideMask-1)*spaces;
        ixRep(1:spaces+1:spaceSize) = 1:wideMask;
        ixRep(ixRep==0) = wideMask+1;    
        sM = zeros(spaceSize,spaceSize,jumpMask);
        for k=1:jumpMask
            tM = rM(:,:,k);
            tM(:,end+1:end+spaces) = 0;
            tM = tM(:,ixRep);
            tM(end+1:end+spaces,:) = 0;
            tM = tM(ixRep,:);
            sM(:,:,k) = tM;
        end
    else
        sM = rM;
    end
end